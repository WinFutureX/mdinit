; mega drive init code
; this is how sega did it

startup:
	tst.l	$A10008			; check control ports A and B
	bne.s	init_portaok		; check soft reset
	tst.w	$A1000C			; check control port C

init_portaok:
	bne.s	softboot		; if soft reset
	lea	init_table(pc), a5	; load init values
	movem.w	(a5)+, d5-d7
	movem.l	(a5)+, a0-a4
	move.b	-$10FF(a1), d0		; get hw ver at $A10001
	andi.b	#$F, d0
	beq.s	init_skiptmss		; if MD is japanese
	move.l	#"SEGA", $2F00(a1)	; make the TMSS happy

init_skiptmss:
	move.w	(a4), d0		; vdp status reg dummy read
	moveq	#0, d0
	movea.l	d0, a6			; a6.l = $0
	move.l	a6, usp			; clear user stack pointer
	moveq	#23, d1			; vdp regs + 1

init_vdp:
	move.b	(a5)+, d5		; prepare regs
	move.w	d5, (a4)		; set regs
	add.w	d7, d5			; next reg
	dbf	d1, init_vdp

init_dmafill:
	move.l	(a5)+, (a4)		; prepare dma fill
	move.w	d0, (a3)		; start dma fill

init_z80:
	move.w	d7, (a1)		; request z80 bus
	move.w	d7, (a2)		; reset z80

init_z80wait:
	btst.b	d0, (a1)		; check if bus granted
	bne.s	init_z80wait
	moveq	#37, d2			; z80 startup code size

init_z80loop:
	move.b	(a5)+, (a0)+		; copy z80 startup code
	dbf	d2, init_z80loop

init_z80end:
	move.w	d0, (a2)		; reset z80
	move.w	d0, (a1)		; return z80 bus
	move.w	d7, (a2)		; reset off

init_clrram:
	move.l	d0, -(a6)		; clear all 64k of ram
	dbf	d6, init_clrram

init_clrcram:
	move.l	(a5)+, (a4)		; set new regs
	move.l	(a5)+, (a4)		; set cram write mode
	moveq	#$1F, d3		; cram size

init_clrcramloop:
	move.l	d0, (a3)		; clear cram 
	dbf	d3, init_clrcramloop

init_clrvsram:
	move.l	(a5)+, (a4)		; set vsram write mode
	moveq	#$13, d4		; vsram size

init_clrvsramloop:
	move.l	d0, (a3)		; clear vsram
	dbf	d4, init_clrvsramloop

init_writepsg:
	moveq	#3, d5			; num of psg channels + 1

init_writepsgloop:
	move.b	(a5)+, $11(a3)		; silence psg channels
	dbf	d5, init_writepsgloop

init_finish:
	move.w	d0, (a2)		; reset z80
	movem.l	(a6), d0-a6		; clear all regs
	move.w	#$2700, sr		; ints off

softboot:
	bra.s	main			; main program code

init_table:
	dc.w	$8000			; d5: vdp register base
	dc.w	$3FFF			; d6: ram clear times
	dc.w	$100			; d7: vdp register increment
	dc.l	$A00000			; a0: z80 ram
	dc.l	$A11100			; a1: z80 busreq
	dc.l	$A11200			; a2: z80 reset
	dc.l	$C00000			; a3: vdp data
	dc.l	$C00004			; a4: vdp ctrl
	dc.b	$04			; vdp reg $80: hblank off, 512 colour mode
	dc.b	$14			; vdp reg $81: md mode 5, dma enabled
	dc.b	$30			; vdp reg $82: plane a nametable @ $C000
	dc.b	$3C			; vdp reg $83: window nametable @ $F000
	dc.b	$07			; vdp reg $84: plane b nametable @ $E000
	dc.b	$6C			; vdp reg $85: sprite table @ $D800
	dc.b	$00			; vdp reg $86: 128k sprite table (unused)
	dc.b	$00			; vdp reg $87: background colour
	dc.b	$00			; vdp reg $88: master system hscroll (unused)
	dc.b	$00			; vdp reg $89: master system vscroll (unused)
	dc.b	$FF			; vdp reg $8A: hblank counter
	dc.b	$00			; vdp reg $8B: ext int off, full screen vscroll & hscroll
	dc.b	$81			; vdp reg $8C: 320 pixel wide display, no interlace, no shadow/highlight
	dc.b	$37			; vdp reg $8D: hscroll table @ $DC00
	dc.b	$00			; vdp reg $8E: 128k plane a/b nametable addr (unused)
	dc.b	$01			; vdp reg $8F: vdp addr increment
	dc.b	$01			; vdp reg $90: 64x32 cell plane size
	dc.b	$00			; vdp reg $91: window hpos
	dc.b	$00			; vdp reg $92: window vpos
	dc.b	$FF			; vdp reg $93: dma length low
	dc.b	$FF			; vdp reg $94: dma length high
	dc.b	$00			; vdp reg $95: dma source low
	dc.b	$00			; vdp reg $96: dma source mid
	dc.b	$80			; vdp reg $97: dma source high + dma type
	dc.l	$40000080		; dma fill to clear vram

init_z80code:
	dc.b	$AF			; xor	a
	dc.b	$01, $D9, $1F		; ld	bc, 1fd9h
	dc.b	$11, $27, $00		; ld	de, 0027h
	dc.b	$21, $26, $00		; ld	hl, 0026h
	dc.b	$F9			; ld	sp, hl
	dc.b	$77			; ld	(hl), a
	dc.b	$ED, $B0		; ldir
	dc.b	$DD, $E1		; pop	ix
	dc.b	$FD, $E1		; pop	iy
	dc.b	$ED, $47		; ld	i, a
	dc.b	$ED, $4F		; ld	r, a
	dc.b	$D1			; pop	de
	dc.b	$E1			; pop	hl
	dc.b	$F1			; pop	af
	dc.b	$08			; ex	af, af'
	dc.b	$D9			; exx
	dc.b	$C1			; pop	bc
	dc.b	$D1			; pop	de
	dc.b	$E1			; pop	hl
	dc.b	$F1			; pop	af
	dc.b	$F9			; ld	sp, hl
	dc.b	$F3			; di
	dc.b	$ED, $56		; im	1
	dc.b	$36, $E9		; ld	(hl), e9h
	dc.b	$E9			; jp	(hl)

init_vdpmodes:
	dc.l	$81048F02		; reg $81: dma off, reg $8F: inc = 2
	dc.l	$C0000000		; cram write
	dc.l	$40000010		; vsram write

init_psgvol:
	dc.b	$9F			; psg ch1 vol: off
	dc.b	$BF			; psg ch2 vol: off
	dc.b	$DF			; psg ch3 vol: off
	dc.b	$FF			; psg ch4 vol: off

main:
	tst.w	$C00004                 ; vdp status reg dummy read (fix reset bug)
	; put your game's main code here
